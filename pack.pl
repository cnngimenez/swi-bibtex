/*   pack
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     04 jun 2017
*/


name(bibtex).
title('Parser and predicates for BibTeX files').
version('0.1.8').
author('Christian Gimenez', 'christian.gimenez@fi.uncoma.edu.ar').
home('https://bitbucket.org/cnngimenez/swi-bibtex/').
download('https://bitbucket.org/cnngimenez/swi-bibtex/get/bibtex-*.zip').
