# BibTeX Package for SWI-Prolog
This package implements predicates and DCG rules for parsing BibTeX files.

# Usage 
This section explain how to use this package. 

For more information, use `doc_browser/0` and, in the Web browser, choose "List extension packs".

## Commands
There are commands implemented for printing BibTeX information to the current output.

In the SWI console, load the library and the BibTeX file with these commands:

```prolog
use_module(library(bibtex_cmd)).
bibtex_use('~/some-bibtex.bib').
```

List all entries using `bibtex_list/0`:

```prolog
bibtex_list.
```

Search for an author and print results:

```prolog
bibtex_search_author("Halpin").
```

Print the N-th entry:

```prolog
bibtex_search_nth(2).
```


## API

Parse an entire BibTeX file.

```prolog
use_module(library(bibtex)).
bibtex_file('~/file.bib', LstEntries).
```

Get the 20-th entry.

```prolog
nth_bibtex_file('~/file.bib', 20, Entry).
```

Search for author's entries.

```prolog
bibtex_author('~/file.bib', "Surname", LstEntries).
```

# Testing
This package does not need to be installed. 

Enter into the `./tests/` directory, run the `swipl` interpreter and execute:

```prolog
use_module(bibtex_tests).
run_tests.
```

# License 
![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

See COPYING.txt file or [the GNU GPL Licence page](https://www.gnu.org/licenses/gpl.html).
