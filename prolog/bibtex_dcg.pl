/*   bibtex_dcg
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     06 Jun 2020
*/


:- module(bibtex_dcg, [
              fields//1, field//2,
	      entry//1,
              author//2, authors//1,
              k_sep/1,
              keyword_spaces//1,
              keyword_sep//1
	  ]).
/** <module> bibtex_dcg: BibTeX DCG rules.

DCG rules that can parse BibTeX elements.

In this text, a pseudo-EBNF syntax is used to explain some structures.

# BibTeX entry format and token names

The following illustrates a BibTex entry and its format. The name of each element is used as a DCG rule's predicate name.

```
"@" name "{" label ","
    field ","
    field ","
    ...
"}"
```

Each field's format is as follows:

```
key "= {" value "},"
```

## Important fields
Fields like author and keywords have their own format. DCG rules for them are
included here. However, consult the bibtex_fields library for more information.

## Author value
The author field format is as follows:

```author "and" author "and" author "and" ...``` 

There are two possible formats for each author's name:

- `surname "," name`
- `name surname`

## Keywords
Keywords are phrases written in the article. There are other fields that use 
the same syntaxs for user-defined keywords.

The format is the same:

```a_phrase "," a_phrase "," ...```

The keywords separators supported are "," and ";". They can be mixed.

# Important terms
- entry/3 is `entry(Name: term, Label: string, LstField: list)`.
- field/2 is `field(Key: atom, Value: string)`.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module(library(dcg/basics)).

/**
   entry(-Entry:pred)// 

Return an entry/3 term corresponding with the codes parsed.

@param Entry `entry(Name:atom, Label:string, Lst: list)`.
@see field//2
*/
entry(entry(AEntryName, SLabel, Lst)) -->
    blanks, "@", string_without("{",EntryName), blanks, "{",
	       string_without(",", Label), blanks, ",",
	       fields(Lst), blanks,
	       "}",
	       {atom_codes(AEntryName, EntryName),
		string_codes(SLabel, Label)}.

/**
   field(-Key:codes, -Value:codes)// is det.

Return the Key = Value field from a bibtex field.
*/
field(Key, Value) --> string_without(" =", Key),
		      whites, "=", whites,
		      value(Value).


inside_value(Value) --> "{", !, inside_value(Val1), "}", inside_value(Val2),
                      { append([`{`, Val1, `}`, Val2], Value) }.

inside_value(Value) --> [C], { C \= 0'} }, !, inside_value(R),
                           { append([[C], R], Value) }.
inside_value([]) --> [].
                   
/**
    value(-Value: codes)//

Parse and retrieve the field's value. The value is returned without any parsing.
Consider using a function from bibtex_fields library.

@param Value the value of the field.
@see field//2
*/
value(Value) --> "{", !, inside_value(Value), "}". %% red cut
%% value(Value) --> "{", !, string_without("}", Value), "}". % red cut
%% word-only value, but is not the last if " ,\n\t" is founded, at the end of the entry if "}" founded.
value(Value) --> string_without(" ,}\n\t", Value), !.


/**
   fields(-Lst:list)// is det.

Return a list of field/2 terms parsed from a bibtex fields string. 
Each element format is `field(+Key: atom, +Value: string)`.
*/
fields(Lst) --> blanks, field(Key, Value), blanks, ",",!, blanks,
		fields(LstRest),
		{atom_codes(AKey, Key), string_codes(SValue, Value),
		    append([field(AKey, SValue)], LstRest, Lst)}.
fields([field(AKey, SValue)]) --> blanks, field(Key, Value), blanks,
				  {atom_codes(AKey, Key), string_codes(SValue, Value)}.

%% --------------------
%% Author's name parsing
%% --------------------

/**
   authorname(Name)//

Is true if ` and ` is not part of the Name. 

Dividing like this will make a more recent backtrack instead backtracking a
whole predicate like author//2.
*/
authorname(Name), " and" --> string(Name), blank, blanks, "and",
			     {\+ append([_, ` and`, _], Name)},!.
authorname(Name) --> string(Name), eos,
		     {\+ append([_, ` and`, _], Name)},!.


/**
 author_names(-Names: codes, -Surname: codes)//

Split the string into names and surname. It is considered that the last word
is the surname.

For example: `J. R. Tolkien` is spitted into `J. R.` as Name and `Tolkien` as
Surname.

@param Names A code with all the names.
@param Surname A code with the surname.
*/
author_names([], Surname), ` and` --> string_without(" ", Surname),
                                    blank, blanks, `and`, !.
author_names([], Surname) --> string_without(" ", Surname),
                            blanks, eos, !.
author_names(Name, Surname) --> string_without(" ", Name1), white, whites,
                              author_names(Rest, Surname),
                              {
                                  %% Avoid the space on the last name
                                  Rest \= [],
                                  append([Name1, ` `, Rest], Name) ;

                                  Name1 = Name
                              }.

/**
   author(-Surname: list, -Name: list)//

Two types of author field's value: 

* Surnames, Names
* Name Surname
* Names Surname

*/
author(Surname, Names) --> string_without(",", Surname), ",", !,
                        whites, authorname(Names).
author(Surname, Names), ` and` --> author_names(Names, Surname), blank, blanks, `and`, !.
author(Surname, Names) --> author_names(Names, Surname), blanks, eos, !.


/**
   authors(-Lst: list)//

Process a list of authors (i.e. author field value). Use it with a value string only.

@param Lst is a list of author/2 terms: author(Surname:string, Name:string).
*/
authors(Lst) --> author(Surname, Name), blank, blanks, "and", !, blank, blanks, authors(LstRest),
		 {string_codes(SurnameS, Surname), string_codes(NameS, Name),
		  append([author(SurnameS, NameS)], LstRest, Lst)}.
authors([author(SurnameS, NameS)]) --> author(Surname, Name),
				       {string_codes(SurnameS, Surname),
					string_codes(NameS, Name)}.

%% --------------------
%% Keywords parsing
%% --------------------

/**
   k_sep(?Char: char).

True iff char is considered as separators.
*/
k_sep(0',).
k_sep(0';).

/**
   keyword_separator//

Parse any separator declared in k_sep/1.
*/
keyword_separator --> [C], {k_sep(C)}. 

/**
   keyword_sep(-Keywords: list)//

True iff Keywords is a list of keywords parsed from the input. The separator
should be one of k_sep/1.

@see keyword_spaces//1
@param Keywords a list of strings.
*/
keyword_sep(Keywords) -->
    whites, string_without(";,", S), whites, keyword_separator,!,
    keyword_sep(Rest),
    {string_codes(SS, S),
     append([SS], Rest, Keywords)}.
keyword_sep([SS]) --> whites, string_without(";,", S), whites, eos,
		     {string_codes(SS, S)}.

/**
   keyword_spaces(-Keywords: list)//

Retrieve all the keywords using spaces as separator. 

@param Keywords is a list of string codes.
*/
keyword_spaces(Keywords) -->
    whites, string_without(" \t", S), white, !, keyword_spaces(Rest),
    {string_codes(SS, S),
     append([SS], Rest, Keywords)}.
keyword_spaces([SS]) -->
    whites, string_without(" \t", S), whites, eos,
    {string_codes(SS, S)}.

