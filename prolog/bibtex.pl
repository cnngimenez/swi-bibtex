/*   bibtex
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     02 jun 2017
*/


:- module(bibtex, [
	      bibtex_file/2,
	      nth_bibtex_file/3,
	      bibtex_author/3,
              bibtex_get_entries/3
	  ]).
/** <module> bibtex: Parse a bibtex file or entry

Predicates that works on a whole BibTeX file. For example, return a particular entry, search for an author's entries or parse the file completely.

# Useful compound terms

- entry/3 is used as a BibTeX entry (a "@" entry in the file). entry(+Name: term, +Label: string, FieldList: list) where FieldList are field/2 terms.
- field/2 is used as a fields key-value from a BibTeX entry. field(+Key: atom, Value: String).

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module(library(dcg/basics)).
:- use_module(bibtex_dcg).


/**
   bibtex_file(+Path: term, -LstEntries: list)

Parse a whole file and retrieve its entries.

This is a slower predicate because it reads all the file into string codes. For
bigger files use nth_bibtex_file/3.
*/
bibtex_file(Path, LstEntries) :-
    read_file_to_codes(Path, Codes, []),
    parse_bibtex(Codes, LstEntries).

parse_bibtex(``, []) :- !.
parse_bibtex(Codes, [Entry|Rest]) :-
    entry(Entry, Codes, RestCodes),!, %% try your best!
    parse_bibtex(RestCodes, Rest).
parse_bibtex(_Codes, []). %% if entry//1 is false, end.

/**
   nth_bibtex_file(+Path: term, +Number:int, -Entry: term) is det.

Search the nth arroba character and read that entry. This method is faster
than parsing the whole file and retrieve then nth element of the list.


@param Number An integer where 1 is the first entry.
@return false if there's no nth entry or file couldn't be readed.
*/
nth_bibtex_file(Path, Number, Entry) :-
    open(Path, read, Stream),
    skip(Stream, `@`),
    nth_bibtex_file_s(Stream, Number, Entry),
    close(Stream).

/**
   nth_bibtex_file_s(+Stream, +Number: int, -Entry: term) is det.

Same as nth_bibtex_file/3, but using a stream.
*/
nth_bibtex_file_s(Stream, 1, Entry) :-
    (seek(Stream, 0, current, 0) ; %% if current position is 1 , do nothing.
     seek(Stream, -1 , current, _NewPos)), %% else seek -1.
    read_entry(Stream, Codes), !,
    entry(Entry, Codes, _Rest).
nth_bibtex_file_s(Stream, Number, Entry) :-
    Number > 1,
    seek(Stream, 1, current, _),
    skip(Stream, `@`),
    Number2 is Number - 1,
    nth_bibtex_file_s(Stream, Number2, Entry).

/**
   skip_up_to_arroba(+Stream).

Move the stream up to arroba, ready for reading it.
*/
skip_up_to_arroba(Stream) :-
    skip(Stream, `@`),!,
    seek(Stream, -1, current, _NewPos).

/**
   read_entry(+Stream, -Codes: list) is det.

   Read one entry from the stream (i.e. from '@' up to '@' or EOF).
*/
read_entry(Stream, [64|Rest]) :-
    peek_code(Stream, 64), !,
    get_code(Stream, 64),
    read_up_to_arroba(Stream, Rest).

read_up_to_arroba(Stream, []) :-
    at_end_of_stream(Stream), !. % Red cut
read_up_to_arroba(Stream, []) :-
    peek_code(Stream, 64),!. % Red cut
read_up_to_arroba(Stream, [C|Rest]) :-
    get_code(Stream, C),
    read_up_to_arroba(Stream, Rest).

/**
   bibtex_author(+File: term, +Author: string, -BibEntries: list)

Search in the BibTeX file all BibTeX entries related to the given author.

@param File the filename.
@param Author a string with the author name or surname word (just a word!).
@param BibEntries a list of entry/3.
*/
bibtex_author(File, Author, BibEntries) :-
    string_codes(Author, AuthorC),
    bibtex_author_int(File, AuthorC, 1, BibEntries).

/**
   has_word(+Word: codes, +Codes: codes) is det

True iff Word is part of Codes.
*/
has_word(Word, Codes) :-
    append([_, Word, _], Codes), !.

bibtex_author_int(File, _Author, N, []) :-
    \+ nth_bibtex_file(File, N, _), !. %% No more entries.
bibtex_author_int(File, Author, N, [entry(Name, Label, LstFields)|Rest]) :-
    nth_bibtex_file(File, N, entry(Name, Label , LstFields)),
    member(field('author', Value), LstFields),
    string_codes(Value, ValueC),
    has_word(Author, ValueC),!, %% red cut
    N2 is N + 1,
    bibtex_author_int(File, Author, N2, Rest).
bibtex_author_int(File, Author, N, Rest) :-
    %% N-th entry hasn't the Author word.
    N2 is N + 1,!,
    bibtex_author_int(File, Author, N2, Rest).

/**
 lst_entries(Lst: list)

The list of entries collected so far.
*/
:- dynamic lst_entries/1.

/**
 add_entry_if_in_list(+Lst_keys: list, +Entry: term)

If the entry key is in the list of keys, then add it to the lst_entries/1
dynamic predicate. Else, just do not add it. Always return true.

@param Lst_keys A list of string keys.
@param Entry The BibTeX entry/3 term.
*/
add_entry_if_in_list(Lst_keys, entry(Key, Label, Fields)) :-
    atom_string(Label, LabelS),
    member(LabelS, Lst_keys),!,
    lst_entries(Lst),
    Lst2 = [entry(Key, Label, Fields)|Lst],
    retractall(lst_entries),
    asserta(lst_entries(Lst2)), !. %% Once asserted, don't regret it!
add_entry_if_in_list(_Lst_keys, _Entry) :- !.

/**
 collect_next_entry(+Stream: term, +Lst_keys: list)

Go to the next BibTeX entry on the stream and add it to lst_entries/1 if its
key is on the Lst_keys string.
*/
collect_next_entry(Stream, _Lst_keys) :-
    at_end_of_stream(Stream), !. % red cut!
collect_next_entry(Stream, Lst_keys) :-
    % is not at EOF and...
    read_entry(Stream, Codes), !,
    entry(Entry, Codes, _Rest),
    add_entry_if_in_list(Lst_keys, Entry).

jump_to_next_entry(Stream) :-
    skip(Stream, `@`),
    
    %% Just end if Stream is at EOF.
    (at_end_of_stream(Stream), ! ;
     seek(Stream, -1, current, _New_pos)).

/**
 bibtex_collect_entries(+Stream: term, +Lst_keys: list)

Get the next entry adding it to the lst_entries/1 if its key is on the
list of keys. Repeat the process until the end of file.
*/
bibtex_collect_entries(Stream, Lst_keys) :-
    repeat,

    jump_to_next_entry(Stream),
    collect_next_entry(Stream, Lst_keys),
    at_end_of_stream(Stream).

/**
 bibtex_get_entries(+Path: term, +Lst_keys: list, -Lst_entries: list) is det.

Search for the given BibTeX keys in the file and return their parsed entries.

Simmilar to nth_bibtex_file/3, walk through the file jumping from entry to entry
and checking each one if its key is in the list. If the file is large, this is
supposed to save more RAM memory than using bibtex_file/2.

@param Path The filename path.
@param Lst_keys A list of strings with the keys to search on the file.
@param Lst_entries A list of entry/3 terms.
*/
bibtex_get_entries(Path, Lst_keys, Lst_entries) :-
    % lst_entries starts empty
    retractall(lst_entries),
    asserta(lst_entries([])),

    open(Path, read, Stream),
    bibtex_collect_entries(Stream, Lst_keys),
    close(Stream),

    % And the collected entries is Lst_entries
    lst_entries(Lst_entries1), !,
    reverse(Lst_entries1, Lst_entries).
