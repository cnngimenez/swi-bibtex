/*   bibtex_cmd
     Author: poo.

     Copyright (C) 2018 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 jun 2018
*/


:- module(bibtex_cmd, [
	      bibtex_use/1,
	      bibtex_search_author/1,
	      bibtex_search_nth/1,
	      bibtex_list/0
	  ]).
/** <module> bibtex_cmd: Commands for reading and writing BibTeX.


*/

:- use_module(library(bibtex)).
:- use_module(library(bibtex_create)).
:- use_module(library(ansi_term)).

:- dynamic bibtex_default/1.

/**
   bibtex_use(+Path: term) is det

Declares that we're using the given BibTeX file.

This predicate can be called more than once.

@param Path A term with the path of the file.
*/
bibtex_use(Path) :-
    asserta(bibtex_default(Path)).

/**
   print_field(+Field: pred) is det.

Prints a BibTeX field.

Note: Displays differently some common fields.

@param Field A field/2 predicate.
*/
print_field(field(author, Value)) :- !,
    ansi_format([bold], '  author: ~w', [Value]), nl.
print_field(field(title, Value)) :- !,
    ansi_format([bold], '  title: ~w', [Value]), nl.
print_field(field(Name, Value)) :-
    format('  ~w: ~w', [Name, Value]), nl.

/**
   print_entry(+Entry: pred) is det

Prints a BibTeX entry into current output.

@param Entry An entry/3 predicate.
*/
print_entry(entry(Type, Key, Fields)) :-
    ansi_format([fg(green)], '~w:~w', [Type, Key]), nl,
    maplist(print_field, Fields).

/**
   bibtex_search_author(+Name: string) is nondet

Displays all the BibTeX entries which authors has Name as substring.

If more than one BibTeX file is declared using bibtex_use/1, then this command is non-deterministic. Otherwise, it is det.

@param Name A string with a word.
*/
bibtex_search_author(Name) :-
    bibtex_default(Path),
    bibtex_author(Path, Name, Lst),
    maplist(print_entry, Lst).

bibtex_search_nth(Nth) :-
    bibtex_default(Path),
    nth_bibtex_file(Path, Nth, Lst),
    maplist(print_entry, Lst).

bibtex_list :-
    bibtex_default(Path),
    bibtex_file(Path, Lst),
    maplist(print_entry, Lst).
