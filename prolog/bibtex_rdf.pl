/*   bibtex_rdf
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     15 jun 2017
*/


:- module(bibtex_rdf, [
	      guess_sufix/3,
	      guess_subject/2,
	      bibtex_to_rdf/2,
	      bibtexfile_to_rdf/2
	  ]).
/** <module> bibtex_rdf: BibTex to RDF port.

Predicates for porting a BibTeX file or entry to its RDF simile.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- ensure_loaded(library(semweb/rdf_db)).
:- ensure_loaded(library(dcg/basics)).
:- ensure_loaded(library(bibtex)).

:- dynamic paper_prefix/1, author_prefix/1.
	   
/**
   key_prop(?BibKey:term, ?RDFProp:term)

A map between a BibTeX key and its RDF property.
*/
key_prop(author, dc:creator).
key_prop(title, rdfs:label).
key_prop(title, dc:title).
key_prop(publisher, dc:publisher).

/**
   prefix(?Prefix:term, ?URL:term)

RDF prefix used.
*/
prefix(dc, 'http://purl.org/dc/elements/1.1/').
prefix(rdfs, 'rdfs: <http://www.w3.org/2000/01/rdf-schema#').

/**
   replace_spaces(-Out: codes)//

True iff Out is the same as the input but all its spaces replaced with "_" symbol, and nothing else that lowercase letters.
*/
replace_spaces([]) --> eos.
replace_spaces(S) --> white, whites,!, % red cut
		      replace_spaces(Rest),
		      {append(`_`, Rest, S)}.
replace_spaces(S) --> alpha_to_lower(C),!, % red cut
		      replace_spaces(Rest),
		      {append([C], Rest, S)}.
replace_spaces([]) --> [_].


/**
   guess_sufix(+Author: term, +Title: string, -Sufix: term)

Try to create the IRI Suffix from the author a title of a bibtex.
*/
guess_sufix(Author, Title, Sufix) :-
    string_codes(Title, TitleCWithSpaces),
    replace_spaces(TitleCNoSpaces, TitleCWithSpaces, _),
    atom_codes(TitleA, TitleCNoSpaces),
    atomic_list_concat([Author, '-', TitleA], Sufix).

/**
   try_paper_prefix(+Sufix: term, +AbbrvURL: term)

Try to use the Prefix if paper_prefix/1 has one defined, if not use the Sufix itself.

paper_prefix/1 is a dynamic predicate.
*/
try_paper_prefix(Sufix, Prefix:Sufix) :-
    paper_prefix(Prefix),!. % red cut.
try_paper_prefix(Sufix, Sufix).

first_author([Author|_Rest], Author) :- !.
first_author([Author], Author) :-!.

/**
   guess_subject(+BibEntry: term, -Subject: term) is det.
   guess_subject(+BibEntry: term, +Subject: term) is det.

True iff Subject is a guessed abbreviated URI we should use for the provided BibEntry.

We need a dynamic predicate paper_prefix/1 defined if a prefix should be used for Subject.
*/
guess_subject(entry(_EName, _Label, Fields), Subject) :-
    member(field(author, Value), Fields),
    author_field(field(author, Value), LstAuthors),!,
    first_author(LstAuthors, author(Surname, _AName)),
    atom_string(SurnameA, Surname),
    member(field(title, Title), Fields),!,
    guess_sufix(SurnameA, Title, Sufix),
    try_paper_prefix(Sufix, Subject).

/**
   bibtex_to_rdf(+BibEntry: term, +Graph: term) is det.

Assert all BibTeX fields into the RDF Graph.
*/
bibtex_to_rdf(BibEntry, Graph) :-
    guess_subject(BibEntry, Subject),
    rdf_assert(Subject, rdf:type, foaf:'Document', Graph),
    take_data(Subject, BibEntry, Graph),!.

/**
   take_data(+Subject: term, +Entry: term, +Graph: term)

Take important data from the Entry and store it in the semantic graph.

@param Entry A BibEntry entry/3.
@param Graph An RDF Graph, see rdf_create_graph/1.
*/
take_data(Subject, entry(_Name, _Label, Fields), Graph) :-
    take_authors(Subject, Fields, Graph),
    take_title(Subject, Fields, Graph).

/**
author_suffix(+AuthorT: term, -AuthorA: term) is det.

True when AuthorA is the suffix needed for the RDF URI to reffer the provided author.

@param AuthorT a author/2 term: author(Surname: string, Name: string).
@param AuthorA a simple term.
*/
author_suffix(author(SurnameS, NameS), AuthorA) :-
    string_codes(SurnameS, SurnameC), string_codes(NameS, NameC),
    append([SurnameC, ` `, NameC], SurnameName), 
    replace_spaces(AuthorC, SurnameName, _),
    atom_codes(AuthorA, AuthorC),!.

/**
   assert_authors(+Subject: term, +Authors: list) is det.

Associate all the authors from Authors 

@param Subject a term.
@param Authors a list of author/2 terms: author(Surname: string, Name: string).
*/
assert_authors(_S, [], _Graph).
assert_authors(S, [AuthorT|Rest], Graph) :-
    author_suffix(AuthorT, AuthorA),
    assert_one_author(S, AuthorA, Graph),
    assert_authors(S, Rest, Graph).

/**
   assert_one_author(+S: term, +Author: term, +Graph:term) is det.

Assert one Author using the prefix from author_prefix/1. If author_prefix/1 is not defined, don't use preffix.

author_prefix/1 is a dynamic predicate.

*/
assert_one_author(S, Author, Graph) :-
    author_prefix(Prefix), !,
    rdf_assert(S, dc:creator, Prefix:Author, Graph).
assert_one_author(S, Author, Graph) :-
    rdf_assert(S, dc:creator, Author, Graph).

/**
   take_authors(+S: term, +Fields: list, +Graph: term).

Take authors from the BibTex Fields and store it in the provided RDF graph.

@param S the subject term representing the paper.
@param Fields is the BibEntry entry/3 field. 
*/
take_authors(S, Fields, Graph) :-
    member(field(author, Value), Fields),
    author_field(field(author, Value), Authors),
    assert_authors(S, Authors, Graph).

/**
   take_title(+Subject: term, +Fields: list, +Graph: term).

Add the title to the 
*/
take_title(Subject, Fields, Graph) :-
    member(field(title, Value), Fields),
    rdf_assert(Subject, dc:title, literal(type(rdfs:string, Value)), Graph),
    rdf_assert(Subject, rdfs:label, literal(type(rdfs:string, Value)), Graph).

/**
   bibentries_to_rdf(+BibEntries:list, +Graph:term)

Process a list of BibEntries with bibtex_to_rdf/2.
*/
bibentries_to_rdf([], _).
bibentries_to_rdf([BibEntry|Rest], Graph) :-
    bibtex_to_rdf(BibEntry, Graph),!,
    bibentries_to_rdf(Rest, Graph).

/**
   bibtexfile_to_rdf(+BibtexFile: term, +Graph: term)

Parse the file and store all BibEntries in the Graph.

@param BibtexFile A term with the BibTeX file path.
@param Graph A RDF graph created with rdf_create_graph/1.
*/
bibtexfile_to_rdf(BibtexFile, Graph) :-
    bibtex_file(BibtexFile, LstBibentries),
    bibentries_to_rdf(LstBibentries, Graph).
