/*   bibtex_fields
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     06 Jun 2020
*/


:- module(bibtex_fields, [
	      author_field/2,
              all_keywords/2,
	      bibentry_keywords/2
	  ]).
/** <module> bibtex_fields: Parsing some BibTeX fields

Some fields can be parsed into smaller and simpler data. This module focus on
some important and frequently used fields.

@author Christian, Gimenez
@license GPLv3
*/

:- use_module(bibtex_dcg).

/**
   author_field(+Field: term, Authors: list)

Parse an author field.

@param Field a field/2 term: field(+Author:term, +Value: String).
@param Authors a list of author/2 terms: author(+Surname: string, +Name: string).
*/
author_field(field(author, Value), AuthorList) :-
    string_codes(Value, ValueL),
    authors(AuthorList, ValueL, _Rest).



/**
   all_keywords(+Codes: codes, -Keywords: list)

True if Keywords is a list with the keywords retrieved from Codes. 

This predicate identify if the codes are separated by ",", ";" or spaces.

@param Codes the value of a "keywords" field.
@param Keywords a list of strings with the keywords.
*/
all_keywords(Codes, Keywords) :-
    k_sep(Sep),
    member(Sep, Codes),!,
    keyword_sep(Keywords, Codes, _).
all_keywords(Codes, Keywords) :-
    keyword_spaces(Keywords, Codes, _).

/**
   bibentry_keywords(+BibEntry: term, -Keywords: list)

return from a BibTeX entry/3 all the keywords declared. 

This predicate parse each "keywords" field's values.

@param Keywords a list of strings.
@param BibEntry an entry/3 compound term.
*/
bibentry_keywords(entry(_Name, _Label, Fields), Keywords) :-
    findall(Value,
	    member(field(keywords, Value), Fields),
 	    KeywordsList),
    flatten_lst_keywords(KeywordsList, Keywords).

/**
   flatten_lst_keywords(+Lst:list, -Keywords:list)

Search for keywords in each string from the given list.

@param Lst A list of string, each one is a list or one keyword.
@param Keywords A list of strings.
*/
flatten_lst_keywords([],[]).
flatten_lst_keywords([Str|Rest], Lst) :-
    string_codes(Str, Codes),
    all_keywords(Codes, Keys),
    flatten_lst_keywords(Rest, RestKeys),
    append(Keys, RestKeys, Lst).
