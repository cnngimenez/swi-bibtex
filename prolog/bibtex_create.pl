/*   bibtex_create
     Author: poo.

     Copyright (C) 2018 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 jun 2018
*/


:- module(bibtex_create, [
	      bibtex_create/2,
	      bibtex_append/2
	  ]).
/** <module> bibtex_create: Predicates for creating a BibTeX file.


*/

/**
   entry_save(+Stream: term, +Entry: pred)

Save a BibTeX entry/3. 
*/
entry_save(Stream, entry(Type, Keyword, Fields)) :-
    format(Stream, '@~w{~w,\n', [Type, Keyword]),
    fields_save(Stream, Fields),
    write(Stream, '}\n').

/**
   fields_save(+Stream: term, +LstFields: list)

Save a list of fields.
*/
fields_save(_Stream, []).
fields_save(Stream, [field(Name, Value)]) :- !,
    format(Stream, '  ~w = {~w}', [Name, Value]).
fields_save(Stream, [field(Name, Value)|Rest]) :-
    format(Stream, '  ~w = {~w},\n', [Name, Value]),
    fields_save(Stream, Rest).

/**
   bibtex_create(+FilePath: term, +Entries: list)

Creates a new BibTeX file storing the given Entries.

@param FilePath A term with the path of the BibTeX file.
@param Entries A list of entry/3 entries.
*/
bibtex_create(File, Entries) :-
    open(File, write, Stream),
    maplist(entry_save(Stream), Entries),
    close(Stream).

/**
   bibtex_append(+FilePath: term, +Entries: list)

Appends the given entries to the provided file.

@param FilePath A term with the path of the BibTeX file.
@param Entries A list of entry/3 entries.
*/
bibtex_append(File, Entries) :-
    open(File, append, Stream),
    maplist(entry_save(Stream), Entries),
    close(Stream).
