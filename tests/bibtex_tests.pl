/*   bibtex-test
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     02 jun 2017
*/


:- module(bibtex_test, [	      
	  ]).
/** <module> bibtex-test: Test for bibtex module

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module('../prolog/bibtex').
:- use_module(data).

:- begin_tests(bibtex, []).

test(bibtex_file) :-
    bibentries(Entries),
    bibtex_file('example.bib', Entries).

test(nth_bibtex_file) :-
    bibentry(halpinb, Entry),
    nth_bibtex_file('./example_b.bib', 3, Entry).

test(nth_bibtex_file_eof, [fail]) :-
    nth_bibtex_file('./example_b.bib', 5, _).

test(bibtex_author) :-
    bibtex_author('./example_b.bib', "Halpin",
		  [entry(book, "halpin:2001",
			 [field('added-at', "2008-07-07T16:45:32.000+0200"),
			  field('author', "Halpin, T."),
			  field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
			  field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
			  field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
			  field('keywords', "imported"),
			  field('publisher', "Morgan Kauffman"),
			  field('timestamp', "2008-07-07T16:45:32.000+0200"),
			  field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
			  field('year',"2001")
			 ]),
		   entry(book, "halpin:2001b",
			 [field('added-at', "2008-07-07T16:45:32.000+0200"),
			  field('author', "Halpin, T."),
			  field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
			  field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
			  field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
			  field('keywords', "imported"),
			  field('publisher', "Morgan Kauffman"),
			  field('timestamp', "2008-07-07T16:45:32.000+0200"),
			  field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
			  field('year',"2001")
			 ])
		  ]).

test(bibtex_get_entries) :-
    bibtex_get_entries('./example_b.bib',
                       ["halpin:2001", "halpin:2001b"],
                       Entries),
    Entries = [
        entry(book, "halpin:2001",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]),
	entry(book, "halpin:2001b",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ])        
    ].



:- end_tests(bibtex).
