/*   bibtex_create_tests
     Author: poo.

     Copyright (C) 2018 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     05 jun 2018
*/


:- module(bibtex_create_tests, [	      
	  ]).
/** <module> bibtex_create_tests: 


*/

:- license(gplv3).

:- use_module('../prolog/bibtex_create').
:- use_module('../prolog/bibtex').

:- begin_tests(bibtex_create, []).

test(bibtex_create) :-
    bibtex_create(
	'./bibtex_create_test.bib',
	[entry(book,
	       "halpin:2001",
	       [field('added-at', "2008-07-07T16:45:32.000+0200"),
		field('author', "Halpin, T."),
		field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
		field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
		field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
		field('keywords', "imported"),
		field('publisher', "Morgan Kauffman"),
		field('timestamp', "2008-07-07T16:45:32.000+0200"),
		field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
		field('year',"2001")])
	]),
    bibtex_file(
	'./bibtex_create_test.bib',
	[entry(book,
	       "halpin:2001",
	       [field('added-at', "2008-07-07T16:45:32.000+0200"),
		field('author', "Halpin, T."),
		field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
		field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
		field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
		field('keywords', "imported"),
		field('publisher', "Morgan Kauffman"),
		field('timestamp', "2008-07-07T16:45:32.000+0200"),
		field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
		field('year',"2001")])
	]).

test(bibtex_append) :-
    bibtex_create('./bibtex_create_test.bib', [
		      entry(book,
			    "halpin:2001",
			    [field('added-at', "2008-07-07T16:45:32.000+0200"),
			     field('author', "Halpin, T."),
			     field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
			     field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
			     field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
			     field('keywords', "imported"),
			     field('publisher', "Morgan Kauffman"),
			     field('timestamp', "2008-07-07T16:45:32.000+0200"),
			     field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
			     field('year',"2001")])
		  ]),
    bibtex_append('./bibtex_create_test.bib', [
		      entry(article,
			    "test:2018",
			    [field(author, "Surname, Name"),
			     field(title, "A test article"),
			     field(year, "2018")])
		  ]),
    bibtex_file('./bibtex_create_test.bib', [
		    entry(book,
			  "halpin:2001",
			  [field('added-at', "2008-07-07T16:45:32.000+0200"),
			   field('author', "Halpin, T."),
			   field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
			   field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
			   field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
			   field('keywords', "imported"),
			   field('publisher', "Morgan Kauffman"),
			   field('timestamp', "2008-07-07T16:45:32.000+0200"),
			   field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
			   field('year',"2001")]),
		    entry(article,
			  "test:2018",
			  [field(author, "Surname, Name"),
			   field(title, "A test article"),
			   field(year, "2018")])
		]).

:- end_tests(bibtex_create).


