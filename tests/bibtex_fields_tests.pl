/*   bibtex_field
     Author: Giménez, Christian.

     Copyright (C) 2020 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     07 Jun 2020
*/


:- module(bibtex_fields_tests, []).
/** <module> bibtex_field: Test cases for bibtex_field module.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module('../prolog/bibtex_fields').
:- use_module(data).

:- begin_tests(bibtex_fields, []).

%% --------------------
%% Authors
%% --------------------

test(author_field) :-
    author_field(field('author', "Halpin, T."), [author('Halpin', 'T.')]),
    author_field(field('author', "Calvanese, Diego and Giacomo, Giuseppe De and Lenzerini, Maurizio and Nardi, Daniele and Rosati, Riccardo"),
		 [author("Calvanese", "Diego"),
		  author("Giacomo", "Giuseppe De"),
		  author("Lenzerini", "Maurizio"),
		  author("Nardi", "Daniele"),
		  author("Rosati", "Riccardo")]),
    author_field(field('author', "Markus Kr{\\\"o}tzsch and
               Frantisek Simancik and
               Ian Horrocks"),
		 [author("Kr{\\\"o}tzsch", "Markus"),
		  author("Simancik", "Frantisek"),
		  author("Horrocks", "Ian")]).

%% --------------------
%% Keywords
%% --------------------

test(all_keywords) :-
    all_keywords(`imported`, ["imported"]),
    all_keywords(`semantic web, RDF, OWLlink, ontology`, ["semantic web", "RDF", "OWLlink", "ontology"]),
    all_keywords(`semantic web; RDF; OWLlink; ontology`, ["semantic web", "RDF", "OWLlink", "ontology"]),
    %% semantic and web are two keywords if space is used as separator (no commas nor semicolon!).
    all_keywords(`semantic web RDF OWLlink ontology`, ["semantic", "web", "RDF", "OWLlink", "ontology"]).

test(bibentry_keywors) :-
    bibentry_keywords(
	entry(book, "halpin:2001b",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]), ["imported"]),
    bibentry_keywords(
	entry(book, "halpin:2001b",
	      [field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('keywords', "second"),
	       field('keywords', "third"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]), ["imported", "second", "third"]),
    bibentry_keywords(
	entry(book, "halpin:2001b",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported, second, third"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]), ["imported", "second", "third"]),
    bibentry_keywords(
	entry(book, "halpin:2001b",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported second third"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]), ["imported", "second", "third"]),
    bibentry_keywords(
	entry(book, "halpin:2001b",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported; second; third"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")
	      ]), ["imported", "second", "third"]).



:- end_tests(bibtex_fields).
