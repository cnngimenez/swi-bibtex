/*   data
     Author: poo.

     Copyright (C) 2020 poo

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     07 Jun 2020
*/


:- module(data, [
              strentry/2, codesentry/2,
              bibentry/2,
              bibentries/1
	  ]).
/** <module> data: Test data.

@author Gimenez, Christian
@license GPLv3
*/

% :- ensure_loaded(library(prosqlite)).
% :- ensure_loaded(library(db_facts)).
% :- use_module(...).

:- license(gplv3).

strentry(halpin, "@book{halpin:2001,
  added-at = {2008-07-07T16:45:32.000+0200},
  author = {Halpin, T.},
  biburl = {http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh},
  interhash = {4fb0f5a53c1f3d9d5da0764bd6a8c09d},
  intrahash = {d9a285b594e7413f253c25ec86186e1c},
  keywords = {imported},
  publisher = {Morgan Kauffman},
  timestamp = {2008-07-07T16:45:32.000+0200},
  title = {Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)},
  year = 2001
}").
strentry(halpinb, "@book{halpin:2001b,
  added-at = {2008-07-07T16:45:32.000+0200},
  author = {Halpin, T.},
  biburl = {http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh},
  interhash = {4fb0f5a53c1f3d9d5da0764bd6a8c09d},
  intrahash = {d9a285b594e7413f253c25ec86186e1c},
  keywords = {imported},
  publisher = {Morgan Kauffman},
  timestamp = {2008-07-07T16:45:32.000+0200},
  title = {Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)},
  year = 2001
}").

codesentry(halpin, Codes) :-
    strentry(halpin, Str),
    string_codes(Str, Codes).

bibentry(halpin,
         entry(book, "halpin:2001",
	       [field('added-at', "2008-07-07T16:45:32.000+0200"),
	        field('author', "Halpin, T."),
	        field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	        field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	        field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	        field('keywords', "imported"),
	        field('publisher', "Morgan Kauffman"),
	        field('timestamp', "2008-07-07T16:45:32.000+0200"),
	        field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	        field('year',"2001")
	       ])
        ).
bibentry(halpinb,
         entry(book, "halpin:2001b",
	       [field('added-at', "2008-07-07T16:45:32.000+0200"),
	        field('author', "Halpin, T."),
	        field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	        field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	        field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	        field('keywords', "imported"),
	        field('publisher', "Morgan Kauffman"),
	        field('timestamp', "2008-07-07T16:45:32.000+0200"),
	        field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	        field('year',"2001")
	       ])
        ).

bibentry(calvanesse,
         entry(inproceedings,"calvaneseGLNR98",
	       [field('added-at', "2012-09-12T00:00:00.000+0200"),
	        field('author', "Calvanese, Diego and Giacomo, Giuseppe De and Lenzerini, Maurizio and Nardi, Daniele and Rosati, Riccardo"),
	        field('biburl', "http://www.bibsonomy.org/bibtex/2b03f555d1821c4d922867497227100b2/dblp"),
	        field('booktitle', "CoopIS"),
	        field('cdrom', "COOPIS1998/83800280.pdf"),
	        field('ee', "http://doi.ieeecomputersociety.org/10.1109/COOPIS.1998.706234"),
	        field('interhash', "92f2faa1c270dba8f7cf86a6abb47114"),
	        field('intrahash', "b03f555d1821c4d922867497227100b2"),
	        field('isbn', "0-8186-8380-5"),
	        field('keywords', "dblp"),
	        field('pages', "280-291"),
	        field('publisher', "IEEE Computer Society"),
	        field('timestamp', "2012-09-12T00:00:00.000+0200"),
	        field('title', "Information Integration: Conceptual Modeling and Reasoning Support."),
	        field('url', "http://dblp.uni-trier.de/db/conf/coopis/coopis98.html#CalvaneseGLNR98"),
	        field('year', "1998")
	       ])
        ).

bibentries([A,B]) :-
    bibentry(halpin, A), bibentry(calvanesse, B).
