/*   bibtex_dcg_tests
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     07 Jun 2020
*/


:- module(bibtex_dcg_tests, [	      
	  ]).
/** <module> bibtex_dcg_tests: Test cases for bibtex_dcg.

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- use_module('../prolog/bibtex_dcg').
:- use_module(data).

:- begin_tests(bibtex_dcg, []).

test(entry) :-
    bibentry(halpin, Entry),
    codesentry(halpin, Codes),
    entry(Entry, Codes, []).
          

test(fields_simple) :-
    string_codes("  author = {Halpin, T.},
      keywords = {imported},
      publisher = {Morgan Kauffman}}", Codes),
    
    fields(
        [field(author, "Halpin, T."),		
	 field(keywords, "imported"),
	 field(publisher, "Morgan Kauffman")],
        Codes, `}`).

test(fields) :-
    string_codes("  added-at = {2008-07-07T16:45:32.000+0200},
  author = {Halpin, T.},
  biburl = {http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh},
  interhash = {4fb0f5a53c1f3d9d5da0764bd6a8c09d},
  intrahash = {d9a285b594e7413f253c25ec86186e1c},
  keywords = {imported},
  publisher = {Morgan Kauffman},
  timestamp = {2008-07-07T16:45:32.000+0200},
  title = {Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)},
  year = 2001
}", Codes),
    
    fields(
        [field('added-at', "2008-07-07T16:45:32.000+0200"),
	 field('author', "Halpin, T."),
	 field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	 field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	 field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	 field('keywords', "imported"),
	 field('publisher', "Morgan Kauffman"),
	 field('timestamp', "2008-07-07T16:45:32.000+0200"),
	 field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	 field('year',"2001")],
        Codes, `}`).

test(field) :-
    string_codes("added-at", Key),
    string_codes("2008-07-07T16:45:32.000+0200", Date),
    string_codes("added-at = {2008-07-07T16:45:32.000+0200},", Field),
    
    field(Key, Date, Field, `,`).


test(value) :-
    bibtex_dcg:value(`testing the {UPCASE} problem in value`,
                     `{testing the {UPCASE} problem in value}`, []),
    bibtex_dcg:value(`{T}esting the {UPCASE} problem in the {VALUE}`,
                     `{{T}esting the {UPCASE} problem in the {VALUE}}`, []).

test(value_curly) :-
    bibtex_dcg:value(`Halpin, T.`, `{Halpin, T.}`, []).

test(value_word) :-
    bibtex_dcg:value(`Halpin`, `Halpin,`, `,`),
    %% Last simple word
    bibtex_dcg:value(`Halpin`, `Halpin}`, `}`).

test(author1) :-
    phrase(author(Surname, Name), `Tolkien, J.`),
    Surname = `Tolkien`,
    Name = `J.`.

test(author2) :-
    phrase(author(Surname, Name), `J. Tolkien`),
    Surname = `Tolkien`,
    Name = `J.`.

test(author3) :-
    phrase(author(Surname, Name), `Tolkien, J. R.`),
    Surname = `Tolkien`,
    Name = `J. R.`.

test(author4) :-
    phrase(author(Surname, Name), `J. R. Tolkien`),
    %% format('|~s|~s|', [Surname, Name]),
    Surname = `Tolkien`,
    Name = `J. R.`.

test(author5) :-
    phrase(author(Surname, Name), `J. R. Tolkien and Name Surname`, Rest),
    %% format('|~s|~s|~s|', [Surname, Name, Rest]),
    Surname = `Tolkien`,
    Name = `J. R.`,
    Rest = ` and Name Surname`.

               
:- end_tests(bibtex_dcg).
