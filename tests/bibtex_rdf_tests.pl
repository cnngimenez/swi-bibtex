/*   bibtex_rdf_tests
     Author: Giménez, Christian.

     Copyright (C) 2017 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     15 jun 2017
*/


:- module(bibtex_rdf_tests, [	      
	  ]).
/** <module> bibtex_rdf_tests: 

@author Gimenez, Christian
@license GPLv3
*/

:- license(gplv3).

:- ensure_loaded(library(semweb/rdf_db)).
:- use_module('../prolog/bibtex_rdf').

:- begin_tests(bibtex_rdf).
test(guess_sufix) :-
    guess_sufix('John', "Bonachon is Garlfield's owner", 'John-bonachon_is_garlfield').

test(guess_subject) :-
    guess_subject(
	entry(book,
	      "halpin:2001",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")]),
	'Halpin-information_modeling_and_relational_databases_').

test(bibtex_to_rdf) :-
    rdf_create_graph(testgraph),
    bibtex_to_rdf(
	entry(book,
	      "halpin:2001",
	      [field('added-at', "2008-07-07T16:45:32.000+0200"),
	       field('author', "Halpin, T."),
	       field('biburl', "http://www.bibsonomy.org/bibtex/2d9a285b594e7413f253c25ec86186e1c/pdeleenh"),
	       field('interhash', "4fb0f5a53c1f3d9d5da0764bd6a8c09d"),
	       field('intrahash', "d9a285b594e7413f253c25ec86186e1c"),
	       field('keywords', "imported"),
	       field('publisher', "Morgan Kauffman"),
	       field('timestamp', "2008-07-07T16:45:32.000+0200"),
	       field('title', "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)"),
	       field('year',"2001")]),
	testgraph),
    S = 'Halpin-information_modeling_and_relational_databases_',
    rdf(S, rdf:type, foaf:'Document', testgraph),
    rdf(S, dc:creator, halpin_t, testgraph),
    rdf(S, dc:title,
	literal(type(rdfs:string, "Information Modeling and Relational Databases (From Conceptual Analysis to Logical Design)")),
	testgraph).
    

			 
:- end_tests(bibtex_rdf).

